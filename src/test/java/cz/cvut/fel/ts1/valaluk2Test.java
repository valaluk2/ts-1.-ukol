package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class valaluk2Test {

    @Test
    public void factorialTest() {

        valaluk2 fac = new valaluk2();

        assertEquals(1, fac.factorial(1));
        assertEquals(1, fac.factorial(0));
        assertEquals(120, fac.factorial(5));

        assertEquals(6, fac.factorial(3));
        assertTrue(true);
    }
}
